#!/bin/sh

if [ -f /run/secrets/controller_key ]; then
  cat /run/secrets/controller_key >> /root/.ssh/id_controller_key
  chmod 600 /root/.ssh/id_controller_key
fi

# start SSH server
exec /usr/sbin/sshd -D
