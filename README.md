﻿# Lab Docker - Ansible


## Setup

Prérequis si licence Home : Windows 10 May 2020 ou ultérieure, avec [extension WSL2 installée](https://docs.microsoft.com/fr-fr/windows/wsl/wsl2-install)

Si licence Pro ou Entreprise : en mode Hyper-V ou en mode WSL2 au choix

1. Installer Docker Desktop (2.2.3.0 version Edge au minimum si on utilise WSL2) puis l'exécuter avec le raccourci.

2. Extraire une copie locale de ce repository

3. Ouvrir le Dashboard à l'aide de l'icone Docker de la zone systray.

4. (mode Hyper-V uniquement) Dans les réglages > Resources > File Sharing, ajouter le répertoire de travail puis appliquer.

5. Ouvrir une invite de commande dans le répertoire de travail

6. `docker-compose build`

7. `docker-compose up` (ne rend pas la main)

8. Dans le dashboard, prendre la main (CLI) sur le conteneur "controller"


## Lancer un playbook

Lister le répertoire courant (/root/ansible_root) avec `ls`

```bash
ansible-playbook ping_all.yml
```

```bash
ansible-playbook report.yml --limit !alpine && jq < ./report.json
```

```bash
ansible-playbook --diff enable.yml
```

On peut modifier les fichiers du sous-répertoire `var/controller/ansible_root` directement sous Windows et exécuter à nouveau le playbook.

(à titre temporaire les répertoires du dossier courant: group_vars et host_vars contiennent des définitions de variables Ansible)

Si on veut ajouter de nouveaux playbooks, il faut les référencer dans le docker-composer.yml.


## Prendre la main sur les VM exemple

Les VM déployées sont **host01** à **host07** avec des OS divers.

`ssh -i /root/.ssh/id_controller_key host03`

## Changer les VM gérées

On peut rajouter des VM supplémentaires dans le fichier `docker-compose.yml`.

Il faut alors penser à les renseigner dans le fichier `var/controller/ansible_root/inventory`


## Tout détruire ?

Dans l'invite de commande : Ctrl+C puis supprimer la stack compose depuis le Dashboard ou avec `docker-compose rm`

(facultatif) Utiliser les commandes `docker image list [--all]` et `docker image prune [--all]` pour faire le ménage
