#!/bin/sh

if [ -f /run/secrets/controller_pub ]; then
  cat /run/secrets/controller_pub >> /root/.ssh/authorized_keys
  chmod 644 /root/.ssh/authorized_keys
fi

# start SSH server
# -e logs to stderr
exec /usr/sbin/sshd -D -e
